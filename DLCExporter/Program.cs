﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DLCExporter
{
    class Program
    {
        const string SERVER = "https://store.playstation.com/chihiro-api/viewfinder";
        const string OUTPUT_PATH = @".\";

        static WebProxy proxy = new WebProxy("127.0.0.1", 1080);

        static void DownloadDLCJson(string gameid, string file)
        {
            var url = $"{SERVER}/{gameid}?relationship=ADD-ONS&sort=release_date&direction=desc&size=100&gkb=1&geoCountry=CN";
            var request = WebRequest.CreateHttp(url);
            request.Accept = "application/json, text/javascript, */*; q=0.01";
            request.Headers["Accept-Encoding"] = "gzip, deflate";
            request.KeepAlive = true;
            request.Referer = "https://store.playstation.com/";
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.89 Safari/537.36";
            request.Headers["X-Requested-By"] = "Chihiro-PSStore";
            request.Headers["X-Requested-With"] = "XMLHttpRequest";
            request.Proxy = proxy;

            Console.WriteLine($"\ndownloading url: {url} ...");
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var stream = response.GetResponseStream();
                var encoding = response.ContentEncoding?.ToLower();
                switch (encoding)
                {
                    case "gzip":
                        stream = new GZipStream(stream, CompressionMode.Decompress, false);
                        break;
                    case "deflate":
                        stream = new DeflateStream(stream, CompressionMode.Decompress, false);
                        break;
                }

                Console.WriteLine($"writing file: {file} ...");
                using (stream)
                using (var fs = File.OpenWrite(file))
                {
                    var buffer = new byte[1024];
                    int count;
                    while ((count = stream.Read(buffer, 0, 1024)) > 0)
                    {
                        fs.Write(buffer, 0, count);
                    }
                }
            }
        }

        static void GenerateCSV(string json, string file)
        {
            Console.WriteLine($"generating CSV file: {file} ...");
            var list = (JArray)JObject.Parse(json)["links"];
            // image: images[0].url
            // id: id
            // name: name
            // date: release_date
            // price: default_sku.price
            // rate: star_rating.score/star_rating.total
            using (var writer = new StreamWriter(file, false, Encoding.UTF8))
            {
                foreach (var item in list)
                {
                    writer.WriteLine($"{item["id"]},{item["name"]},{item["release_date"]},{item["default_sku"]?["price"]}");
                }
            }
        }

        static void Main(string[] args)
        {
            DownloadDLCJson("JP/ja/19/JP0102-CUSA04243_00-60GAMEPACKAGE000", $"{OUTPUT_PATH}dlcs.txt");
            DownloadDLCJson("HK/zh/19/HP0102-CUSA04918_00-ASIAPLACEHOLDER0", $"{OUTPUT_PATH}dlcs_hk.txt");

            GenerateCSV(File.ReadAllText($"{OUTPUT_PATH}dlcs.txt"), $"{OUTPUT_PATH}dlcs.csv");
            GenerateCSV(File.ReadAllText($"{OUTPUT_PATH}dlcs_hk.txt"), $"{OUTPUT_PATH}dlcs_hk.csv");

            Console.WriteLine();
            Console.Write("press ESC to close.");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
        }
    }
}
